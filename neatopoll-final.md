# NeatoPoll Final Report

# Introduction

NeatoPoll is a web-based application that provides an easy way to poll a group of participants. It has basic configuration options and can address a wide range of use cases, for example to run a simple election, to give a pop quiz in a classroom setting, or to rank preferred dates for a party amongst friends.

A user either creates or joins a poll. When a user creates a poll, they are the **Poll Creator**. When a user Joins a poll, they are a **Participant**. Poll Creators set up and run the entire poll; Participants merely answer the poll. Depending on how the poll has been configured, a Participant *may* be permitted to see the results.

A core design goal of NeatoPoll is that it is responsive: a user should easily be capable of creating a poll using their mobile phone, or a teacher using an iPad in a classroom setting. The web pages have thus been designed accordingly.

Half the website has been implemented (where there are screenshots). It's Christmas Eve, I hope you'll give me a break ;-)

The work I did has been uploaded to Google Cloud Platform as a static site and the domain name I bought has a CNAME record pointing to it so this link should work: [www.neatopoll.com](http://www.neatopoll.com/index.html).

# Design and Implementation

[This wireframe prototype](https://app.moqups.com/egarson@gmail.com/RvET445emN/view/page/ad64222d5) provides a high-level overview of the site beginning with the landing page (n.b.: most of the buttons work). Here is a screeshot of this simple page:

### Screenshot

![Landing Page Screenshot](https://storage.googleapis.com/www.neatopoll.com/img/index.png)

The [Site Map](https://app.moqups.com/egarson@gmail.com/RvET445emN/view/page/aef07f34f) shows the overall structure of the site.

The features are described in further detail below, grouped by page.

## Configure Poll

### Screenshot

![Configure Screenshot](https://storage.googleapis.com/www.neatopoll.com/img/configure.png)

To [configure a poll](https://app.moqups.com/egarson@gmail.com/RvET445emN/view/page/af9d4bd78), the Poll Creator gives it a memorable title, a privacy level and decides whether or not to share poll results.

The Privacy Level determines whether or not Participants can see how other Participants voted: **Anonymous** (no-one can see how a *given* Participant voted - including the Poll Creator), **Tracked** (only the Poll Creator can see how *each* Participant voted), or **Open**, whereby everyone can see everyone else's vote.

If the Poll Creator chooses to share results, then after a Participant has voted (and only then) are they able to access the results.

## Add Questions

### Screenshot

![Add Question Screenshot](https://storage.googleapis.com/www.neatopoll.com/img/add.png)

Polls are (of course) composed of one or more poll questions. Poll Creators [add questions](https://app.moqups.com/egarson@gmail.com/RvET445emN/view/page/a0fae3ca4) of different types. In NeatoVote, a question can have one of three types: either **Multiple Choice**, **Free-Text** (the answer is typed into a text field), or **Rank** (i.e. Participants sort a given list of items).

### Multiple Choice Questions

### Screenshot

![Multiple Choice Question Screenshot](https://storage.googleapis.com/www.neatopoll.com/img/multiple.png)

To add a [Multiple Choice question](https://app.moqups.com/egarson@gmail.com/RvET445emN/view/page/ad7c29354), the Poll Creator adds the question text along with two or more choices, starting with the correct one (which indicates the right answer to the system).

Observe that a that a true/false or yes/no question is simply a multiple choice one with only two options, i.e. it is simply a degenerate form. Also, not all multiple choice questions need have a "correct" answer; when they do not, then an opinion is being solicited.

### Free-Text Questions

### Screenshot

![Free-Text Question Screenshot](https://storage.googleapis.com/www.neatopoll.com/img/free.png)

To add a [Free-Text question](https://app.moqups.com/egarson@gmail.com/RvET445emN/view/page/a81cf7459), the Poll Creator adds the question text and the Correct Answer.

It is not straightforward for the system to determine whether a *Participant* got the correct answer, in the face of spacing and capitalization vagaries. Therefore, when the system compares the Correct Answer to the Participant's Given Answer, the following algorithm is used to determine correctness:

1. Remove all leading and trailing spaces from both (the Given and Correct) answers
2. Normalize / reduce all other spaces (i.e. "in between" words) to 1 space from both answers
3. Compare the given answer (as lowercase) to what was submitted (as lowercase)

This algorithm eliminates the possibility of getting the answer wrong because of capitalization or spacing issues. A future version of NeatoPoll could provide the option to do exact matching, but initially, flexibility is warranted. I would implement this on the server side, such that the logic does not need to be re-implemented for every new front end that may be developed in the future.

### Rank Questions

To add a [Rank question](https://app.moqups.com/egarson@gmail.com/RvET445emN/view/page/aaf00de37), the Poll Creator adds the question text and the Correct Answer (items) in their expected order.

## Review Poll

The [Review Poll](https://app.moqups.com/egarson@gmail.com/RvET445emN/view/page/a0aad71b3) stage is next, after the Poll Creator has completed adding questions; here, they are given the chance to review and (potentially) edit them. Once the Poll Creator is satisfied, they open the poll to Participants.

## Open Poll

The Poll Creator can [open the poll](https://app.moqups.com/egarson@gmail.com/RvET445emN/view/page/a537d73f4) after the review stage. The system generates a unique *Join Code* which Participants use to access the newly-configured poll. The poll remains open (or active) until the Poll Creator decides to close it. Once a poll is closed, no more voting can take place.

## Answer Poll

the [Answer Poll](https://app.moqups.com/egarson@gmail.com/RvET445emN/view/page/ab987f693) page is for the Participants, and they get to this page via a Join Code. If the poll is *Tracked* or *Open*, then the Participant's name is required. This page shows what the poll looks like to a participant, in particular how questions are formatted and presented as a function of their Type.

After a Participant submits their answer(s), they are either brought to the Results page (if Sharing is configured) or they are thanked for their participation (and their involvement ends).

## Results Page

The Poll Creator is brought to the [Results page](https://app.moqups.com/egarson@gmail.com/RvET445emN/view/page/a156bd957) after Closing the poll. If the poll was configured to Share Results, users that try to access the Poll at this point are redirected here; otherwise, they are redirected to a page that states that the poll results are **not shared**. (This important detail adds the necessary information to know the difference between a non-existant poll (or:  mis-typed Join Code), and one whose results are not shared).

Note that the [Results page](https://app.moqups.com/egarson@gmail.com/RvET445emN/view/page/a156bd957) also shows the *current* poll results; if the poll is Closed, then implicitly, it is the final result being shown.

This page is clearly a cadidate for Push Notifications, so that it dynamically updates in real time.

## Track Results

The [Track Results page](https://app.moqups.com/egarson@gmail.com/RvET445emN/view/page/ac48037d8) shows how each individual participant responded. It is only available if the poll's Privacy Level is *not Anonymous*. If the Privacy Level is *Tracked*, then only the Poll Creator can see it, and if the Privacy Level is **Open**, then everyone can see it:

# From Design to Code

## Presentation: Cards Cards Everywhere

Having been exposed to a number of Bootstrap components, and given the requirement to be responsive, I opted to use Bootstrap's Card facility. Cards have "powerful display options" and provide a number of presentation sub-components for flexibility.

## Development

I tried to push as much into the stylesheet as possible; this is a work in progress. Centralizing presentation in css helps create a consistent look and feel across the site, and the ability to effect easily global changes.

Testing the site for responsiveness helped me a great deal to learn better how to format pages using the Bootsrap grid system.

Following recent experience using Grunt and Gulp, and being unimpressed with either of them, I opted to stick to simple npm scripts for this project. Based on my limited research, many developers have returned to npm after forays into the vagaries of these tools.

Next time I do this, I would like to try to build the html entirely using markdown, in order to try and DRY out some of the declarations.

# Conclusion

This has been a fun project and as you can see I got carried away.

Thanks for reading this far!!!

Happy holidays

Edward
